package cat.proven.productmanagerapp;

//import java.io.BufferedReader;
//import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/**
 * Class ProductManagerApp, contains the main method. First exercise at module
 * M03 Programming - Java at proven.cat
 *
 * @author Alejandro Asensio<alejandro.asensio.10 at gmail.com>
 * @version 1.0 2018-10-30 Initial version
 * @version 2.0 2018-12-04 Modified: basic arrays converted to Collections
 * (ArrayList)
 */
public class ProductManagerApp {

    Store myStore;
    Scanner sc = new Scanner(System.in);
    private final ArrayList<String> menuOptions = new ArrayList<>();

    /**
     * Constructor. It creates the menu options list.
     */
    public ProductManagerApp() {
        this.menuOptions.add("Exit");
        this.menuOptions.add("List all products");
        this.menuOptions.add("Find product by code");
        this.menuOptions.add("Find product by name");
        this.menuOptions.add("Add new product");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ProductManagerApp myApp = new ProductManagerApp();
        myApp.run();
    }

    /**
     * This method runs the code of the application.
     */
    private void run() {
        myStore = new Store();
        loadData();
        int optionSelected;
        do {
            optionSelected = showMenu();

            switch (optionSelected) {
                case 1:
                    listAllProducts();
                    break;
                case 2:
                    findProductByCode();
                    break;
                case 3:
                    findProductByName();
                    break;
                case 4:
                    addNewProduct();
                    break;
                default:
                    System.out.println("Warning: Choose a valid option.");
            }
            //System.out.format("You have chosen the option %d.\n", optionSelected); //initial debug
        } while (optionSelected != 0);
    }

    /**
     * Shows a menu to the user and asks for an option.
     *
     * @return the selected option or -1 in case of error
     */
    private int showMenu() {
        int option = -1;
        System.out.println("Menu");
        Iterator<String> it = menuOptions.iterator();
        int i = 0;
        while (it.hasNext()) {
            System.out.format("%d. %s\n", i, it.next());
            i++;
        }
        System.out.print("Please, choose an option: ");
        // Usage of try-catch block when a critical error is expected.
        try {
            // Other way to get the user input, with BufferedReader:
//            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//            option = Integer.parseInt(reader.readLine());
            Scanner sc = new Scanner(System.in);
            option = sc.nextInt();
        } catch (Exception e) {
            System.out.println("Error: The option must be an integer.");
        }
        return option;
    }

    /**
     * Loads example data to debug the program.
     */
    private void loadData() {
        myStore.add(new Product("P001", "Keyboard", 19.95));
        myStore.add(new Product("P002", "Pendrive", 29.95));
        myStore.add(new Product("P003", "Monitor", 139.95));
        myStore.add(new Televisor(43, "P004", "Smart TV LED Curve", 1299.95));
        myStore.add(new Product("P005", "Monitor", 69.95));
    }

    /**
     * Lists all the products of the store.
     *
     * @return -1 if the store is empty; 0 otherwise.
     */
    public int listAllProducts() {
        int result = myStore.getProducts().isEmpty() ? -1 : 0;
        if (result == -1) {
            System.out.println("The store is empty.");
        } else {
            Iterator<Product> it = myStore.getProducts().iterator();
            while (it.hasNext()) {
                System.out.println(it.next().toString());
            }
        }
        return result;
    }

    /**
     * Prints the product that matches with the code given by user.
     */
    private void findProductByCode() {
        System.out.print("Code: ");
        String code = sc.nextLine();
        Product p = myStore.findByCode(code);
        if (p == null) {
            System.out.printf("The code '%s' doesn't exist in the Store.\n", code);
        } else {
            System.out.println(p.toString());
        }
    }

    /**
     * Prints a list of products with the name given by user.
     */
    private void findProductByName() {
        System.out.print("Name: ");
        String name = sc.nextLine();
        Store productsFound = myStore.findByName(name);
        if (productsFound.getProducts().isEmpty()) {
            System.out.printf("There is no products with the name '%s' in the Store.\n", name);
        } else {
            Iterator<Product> it = productsFound.getProducts().iterator();
            while (it.hasNext()) {
                System.out.println(it.next().toString());
            }
        }
    }

    /**
     * Asks the user the fields of a new product (or television). If the code
     * that user types already exists in the store, no more fields will be
     * asked; otherwise, all fields will be used to create a new product (or
     * television) to the store.
     */
    private void addNewProduct() {
        System.out.print("Code: ");
        String code = sc.nextLine();
        // Checks if the given code already exists in the store.
        if (myStore.findByCode(code) != null) {
            System.out.println("Warning: This code already exists.");
        } else {
            System.out.print("Name: ");
            String name = sc.nextLine();
            System.out.print("Price: ");
            double price = sc.nextDouble();
            sc.nextLine(); // removes the \n after the double typed
            System.out.print("Is this product a Televisor? [Y/n]: ");
            String isTelevisor = sc.nextLine().toLowerCase();
            if (isTelevisor.equals("y")) {
                System.out.print("Inches: ");
                int inches = sc.nextInt();
                myStore.add(new Televisor(inches, code, name, price));
                System.out.println("Televisor added to store successfully.");
            } else {
                myStore.add(new Product(code, name, price));
                System.out.println("Product added to store successfully.");
            }
        }
    }

}
