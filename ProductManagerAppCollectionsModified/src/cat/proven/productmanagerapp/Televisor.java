package cat.proven.productmanagerapp;

/**
 * Class Televisor, extends from Product
 *
 * @author Alejandro Asensio<alejandro.asensio.10 at gmail.com>
 */
public class Televisor extends Product {

    private int inches;

    // Full constructor, calling super (parent class Product)
    public Televisor(int inches, String code, String name, double price) {
        super(code, name, price);
        this.inches = inches;
    }

    public Televisor(String code) {
        super(code);
    }

    public Televisor(Televisor other) {
        super(other.getCode(), other.getName(), other.getPrice());
        this.inches = other.inches;
    }

    public int getInches() {
        return inches;
    }

    public void setInches(int inches) {
        this.inches = inches;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString()).append(" Televisor: {inches = ").
                append(inches).append("}");
        return sb.toString();
    }

}
