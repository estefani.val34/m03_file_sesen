package cat.proven.productmanagerapp;

import java.util.Objects;

/**
 * Class Product
 *
 * @author Alejandro Asensio<alejandro.asensio.10 at gmail.com>
 */
public class Product {

    private String code;
    private String name;
    private double price;

    // Contractor void
    public Product() {
    }

    // Constructor with code
    public Product(String code) {
        this.code = code;
    }

    // Full initializer constructor
    Product(String code, String name, double price) {
        this.code = code;
        this.name = name;
        this.price = price;
    }

    // Contructor from an existing Product
    public Product(Product p) {
        this.code = p.getCode();
        this.name = p.getName();
        this.price = p.getPrice();
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 7; //arbitrary prime integer
        hash = 61 * hash + Objects.hashCode(this.code); //arbitrary prime integer
        return hash;
    }

    /**
     * Two Products are considered equals if their code is the same.
     *
     * @param o
     * @return true if 'this' or his code is equals to 'o', false otherwise
     */
    @Override
    public boolean equals(Object o) {
        boolean b;
        if (o == null) {
            b = false;
        } else if (o == this) {
            b = true;
        } else if (o instanceof Product) {
            Product p = (Product) o;
            b = this.code.equals(p.code);
        } else {
            b = false;
        }
        return b;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Product: {code = ").append(code)
                .append(", name = ").append(name)
                .append(", price = ").append(price)
                .append("}");
        return sb.toString();
    }

}
