package cat.proven.productmanagerapp;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Class Store
 *
 * @author Alejandro Asensio<alejandro.asensio.10 at gmail.com>
 */
public class Store {

    private ArrayList<Product> products;

    // Constructor
    public Store() {
        this.products = new ArrayList<>();
    }

    // Getters and setters
    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

    /**
     * Finds the given product in the store.
     *
     * @param p a Product
     * @return the found product; null if that product is not in the store
     */
    public Product find(Product p) {
        Product pFound = null;
        Iterator<Product> it = products.iterator();
        while (it.hasNext()) {
            Product currentProduct = it.next();
            if (currentProduct.equals(p)) {
                pFound = currentProduct;
            }
        }
        return pFound;
    }

    /**
     * Finds the product with the given code.
     *
     * @param code
     * @return Product p if the code exists, null otherwise
     */
    public Product findByCode(String code) {
        Product p = null;
        Iterator<Product> it = products.iterator();
        while (it.hasNext()) {
            Product currentProduct = it.next();
            if (currentProduct.getCode().equals(code)) {
                p = currentProduct;
            }
        }
        return p;
    }

    /**
     * Adds the given product to the products list if the code of the product is
     * not yet in the store.
     *
     * @param p product to be added
     * @return 0 if the proudct has been added, -1 otherwise
     */
    public int add(Product p) {
        int result = 0;
        Iterator<Product> it = products.iterator();
        while (it.hasNext()) {
            if (it.next().equals(p)) {
                result = -1;
            }
        }
        if (result == 0) {
            products.add(p);
        }
        return result;
    }

    /**
     * Find all the products with the given name.
     *
     * @param name a product name to find
     * @return a temporary store with the products found, null if there is no
     * products with the given name
     */
    public Store findByName(String name) {
        Store productsFound = new Store();
        Iterator<Product> it = products.iterator();
        while (it.hasNext()) {
            Product currentProduct = it.next();
            if (currentProduct.getName().equals(name)) {
                productsFound.add(currentProduct);
            }
        }
        return productsFound;
    }

}
