/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FileAttributes;

import java.io.File;
import java.util.Date;

/**
 *
 * @author tarda
 */
public class fileAttributes {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        StringBuilder sb= new StringBuilder();
        if (args.length==1) {
            File file =new File(args[0]);
            sb.append("\nRuta: ");
            sb.append(file.getPath());
            sb.append("\nName: ");
            sb.append(file.getName());
            sb.append("\nRuta ablsuta : ");
            sb.append(file.getAbsoluteFile());
            
            if (file.exists()) {
                sb.append("\nPropiedades");
                sb.append(file.isDirectory()?"+d":"-d");//es un if-else 
                sb.append(file.isFile()?"+f":"-f");
                sb.append(file.isHidden()?"+h":"-h");
                sb.append(file.canRead()?"+r":"-r");
                sb.append(file.canWrite()?"+w":"-w");
                sb.append("\nTamany: ");
                sb.append(file.length());
                Date data= new Date(file.lastModified());
                sb.append("\nultima modificacio: ");
                sb.append(data.toString());
                
            }else{
                sb.append("este directorio no existe");
            }
        }else{
            sb.append("usar 1 argumento : FileAttributes <path>");
        }
        System.out.println(sb.toString());
        // poner en el customize la ruta para que te salga 
        
    }
    
}
