package DirList;

/**
 * :~/NetBeansProjects/directoriesFitxers/src/DirList$ java DirList.java /home
 *
 * @author tarda
 */
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.UserPrincipal;

public class DirList {

    public static void main(String[] args) {

        String result = ""; //gets the list of directories
        DirList myAp = new DirList();

        if (args.length > 0) {
            result = myAp.listFilesInDir(args[0]);
        } else {
            result = "Usage: DirList <dir>";
        }

        System.out.println(result);
    } //end of main

    /**
     * listFilesInDir(): Lists the files contained in the given directory.
     *
     * @param str: is the name of the directory to list.
     * @return a string containing the list of directories.
     */
    public String listFilesInDir(String str) {

        File dir = new File(str);
        StringBuilder sb = new StringBuilder();

        if (dir.exists() && dir.isDirectory()) {

            sb.append("\nAbsolute path: ");
            sb.append(dir.getAbsolutePath());
            sb.append("\nRelative path: ");
            sb.append(dir.getPath());
            sb.append("\nDirectory Name: ");
            sb.append(dir.getName());
            sb.append("\nContents 1: ");
            sb.append("\n------------------");

            String[] fileList = dir.list();

            for (int i = 0; i < fileList.length; i++) {
                sb.append("\nName:");
                sb.append(fileList[i]);
                sb.append(" Propietari:");
                sb.append(fileList[i]);
            }

            sb.append("\nContents 2: ");
            sb.append("\n------------------");
            String out="";
            StringBuilder content = new StringBuilder();
            String[] contentArray = null;
            File[] archivosYCarpetasInternos = dir.listFiles();
            for (File archivoOCarpeta : archivosYCarpetasInternos) {
                if (archivoOCarpeta.isHidden()) {
                    content.append("\nName:");
                    content.append(archivoOCarpeta.getName());

                } else {
                    content.append("\nName:");
                    content.append(archivoOCarpeta.getName());

                }
                content.append(" Permisos:");
                content.append(archivoOCarpeta.canRead() ? "+r" : "-r");
                content.append(archivoOCarpeta.canWrite() ? "+w" : "-w");
                content.append(archivoOCarpeta.canExecute() ? "+x" : "-x");
               
                try {

                    String fileName = archivoOCarpeta.toString();
                    Path path = Paths.get(fileName);
                    UserPrincipal propietario = Files.getOwner(path);
                    String username = propietario.getName();
                    content.append(" Propietari:");
                    content.append(propietario);
                    System.out.println("El propietario del archivo : " + fileName + " es : " + propietario);

                } catch (Exception e) {
                    System.out.println("Exception: " + e.toString());
                }
                
                java.util.Date data = new java.util.Date(dir.lastModified());
                content.append(" Data:");
                content.append(data.toString());
                content.append(",");
                out = content.toString();
            }
            contentArray = out.split(",");
            String[] fileList2 = contentArray;

            for (int i = 0; i < fileList2.length; i++) {
                sb.append("\n");
                sb.append(fileList2[i]);
            }
            
            

        }
        return sb.toString();
    }//end of listFilesInDir

} //end of class

/**
 -l
 -a
 * -la
 * 
 */
