/*
 string a person 
person a string 

es una clase auxiliar 
 */
package readwritecsv;

/**
 *
 * @author estefani
 */
public class CsvHelper {

    public String personToCsv(Person p, String delimiter) {
        return String.format("%s%s %s%s %s", p.getNif(), delimiter, p.getName(), delimiter, p.getPhone());
    }

    public Person cvsToPerson(String csvLine, String delimiter) {
        Person p = null;
        String[] tokens = csvLine.split(delimiter);
        
        if (tokens.length == 3) {
            String nif = tokens[0];
            String name = tokens[1];
            String phone = tokens[2];
            p = new Person(nif, name, phone);
        }

        return p;
    }

}
