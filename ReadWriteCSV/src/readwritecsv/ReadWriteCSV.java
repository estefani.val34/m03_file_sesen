/*
Voy a cargar un Arraylist de personas. 
Las personas que he creado se han de guardar en un fichero CSV.
Leer el fichero CSV. 
Separarlo en métodos 
 */
package readwritecsv;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.Iterator;
import java.util.Scanner;

/**
 *
 * 
 * @author estefani
 */
public class ReadWriteCSV {

    private final ArrayList<Person> list1 = new ArrayList<Person>();
    private final ArrayList<Person> list2 = new ArrayList<Person>();

    public static void main(String[] args) {
        ReadWriteCSV myApp = new ReadWriteCSV();
        myApp.run(args);

//        String delimiter = ";";
//        Person p = new Person("1", "e", "33");
//        //from person to csv
//        CsvHelper helper = new CsvHelper();
//        System.out.println(helper.personToCsv(p, delimiter));
//
//        String csv = "111; isa;77";
//        p = helper.cvsToPerson(csv, delimiter);
//        System.out.println(p.toString());
    }

    private void run(String[] args) {
        loadData();
        writefile(args);
        readfile(args);
        list2();
    }

    public void writefile(String[] args) {
        String delimiter = ";";
        CsvHelper helper = new CsvHelper();
        if (args.length == 1) {
            try {
                FileWriter fw = new FileWriter(args[0], true);
                BufferedWriter bw = new BufferedWriter(fw);

                Iterator iter = list1.iterator();

                while (iter.hasNext()) {
                    bw.write(helper.personToCsv((Person) iter.next(), delimiter));
                    bw.write("\n");
                }

                bw.close();

            } catch (Exception e) {
                System.out.println("Error:" + e);
            }

        } else {
            System.out.println("Usage: write the filaname");
        }
    }

    private void loadData() {
        list1.add(new Person("1", "estef", "232323"));
        list1.add(new Person("2", "rod", "34234453"));
        list1.add(new Person("3", "rac", "43534"));

    }

    private void readfile(String[] args) {
        int c;
        String delimiter = ";";
        CsvHelper helper = new CsvHelper();
        ArrayList<String> listString = new ArrayList<String>();
        if (args.length == 1) {
            try {
                //FileReader fr = new FileReader(args[0]);
                BufferedReader br = new BufferedReader(new FileReader(args[0]));
                StringBuilder csv = new StringBuilder();
                while ((c = br.read()) != -1) {
                    csv.append((char) c);
                }

                csv.append("\n");
                listString.add(csv.toString());

                for (String line : listString.get(0).split("\n")) {
                    list2.add(helper.cvsToPerson(line, delimiter));
                }

                br.close();

            } catch (Exception e) {
                System.out.println("Error:" + e);
            }

        } else {
            System.out.println("Usage: write the filaname");
        }
    }

    public void list2() {
        for (Person p : list2) {
            System.out.println(p);
        }
    }

}
