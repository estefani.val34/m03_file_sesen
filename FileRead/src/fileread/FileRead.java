/*
 Llista el contingut d'un directori entrat per la línia d'ordres
 */
package fileread;

import java.io.File;

public class FileRead {

    public static void main(String[] args) {

        FileRead myAp = new FileRead();
        String result = "";
        if (args.length > 0) {
            result = myAp.listFilesInDir(args[0]);
//            for (int i = 0; i < args.length; i++) {
//                System.out.println("Param "+i+":"+args[i]);
//            }
        } else {
            result = "Utilitzacio: java DirList.java";
        }
        System.out.println(result);
    }

    private String listFilesInDir(String arg) {
        //  System.out.println("Paramaetro:"+arg);
        //quiero que me liste los ficheros en /home que es args[0]
        File dir = new File(arg);
        StringBuilder sb = new StringBuilder();
        if (dir.exists() && dir.isDirectory()) {
            sb.append("\nRuta absoluta");
            sb.append(dir.getAbsolutePath());
            sb.append("\nRuta realtiva");
            sb.append(dir.getPath());
            sb.append("\nNOmbre del directorio");
            sb.append(dir.getName());
            sb.append("\nCOntenido: ");
            sb.append("\n------------------- ");
            String[] fileList = dir.list();
            for (int i = 0; i < fileList.length; i++) {
                sb.append("\n");
                sb.append(fileList[i]);
            }
            //run > set project condiguration >costumize , punto ,  /home , /home/tarda/NetBeansProjects
          
        }
        return sb.toString();
    }
}

/*
Para verlo en el terminal :
1-. javac FileRead.java  en el terminal --> me crea en la misma carpeta el .class
2.- java FileRead prametrouno
3.- java FileRead.java parametrouno

Para verlo en el netbeans run > set project condiguration >costumize
 */
