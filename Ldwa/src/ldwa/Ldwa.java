package ldwa;

import java.io.File;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.UserPrincipal;

/**
 * ~/NetBeansProjects/directoriesFitxers/src/DirList$ java Ldwa.java -l /home
 *
 * @author tarda
 */
public class Ldwa {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        File dir;
        String result1 = "";

        Ldwa myAP = new Ldwa();

        if (args.length == 2) {

            result1 = myAP.listFilesInDir(args);

//        } else if (args.length == 1) {
//            String[] args2 = null;
//            args2[0] = args[0];
//
//            String fileName = ".";
//            Path path = Paths.get(fileName);
//            UserPrincipal propietario = Files.String username = propietario.getName();
//            args2[1] = path;
//            result1 = myAP.listFilesInDir(args2);
            //opcio ="-l"
            //File dir=new File(".")

        } else {
            result1 = "Input option [-l, -a, -la] and path, please ";
        }

        System.out.println(result1);
    }

    /**
     * *
     * Lists the files contained in the given directory args[1].
     *
     * @param str
     * @return
     */
    public String listFilesInDir(String[] args) {
        String option = args[0];
        File dir = new File(args[1]);

        StringBuilder sb = new StringBuilder();

        if (dir.exists() && dir.isDirectory()) {

            sb.append("\nAbsolute path: ");
            sb.append(dir.getAbsolutePath());
            sb.append("\nRelative path: ");
            sb.append(dir.getPath());
            sb.append("\nDirectory Name: ");
            sb.append(dir.getName());
            sb.append("\nContents according to the option: ");
            sb.append("\n------------------");

            String[] fileList = listOptionPath(dir, option);

            for (int i = 0; i < fileList.length; i++) {
                sb.append("\n");
                sb.append(fileList[i]);
            }
        }
        return sb.toString();
    }

    /**
     * *
     * Give the content according to the option args[0]. Amb -l mostrarà: ruta,
     * permisos, propietari, data, nom. Amb -a mostrarà: llistat de tots els
     * elements del directori (incloent els fitxers ocults) Amb -la mostrarà:
     * llistat de tots els elements i també les seves propietats (és a dir les
     * dues opcions, -l i -a a la vegada)
     *
     * @param dir
     * @param option
     * @return
     */
    public String[] listOptionPath(File dir, String option) {

        StringBuilder content = new StringBuilder();

        String[] contentArray = null;
        switch (option) {
            case "-l":
                String out = option_l(content, dir);
                contentArray = out.split(",");
                break;
            case "-a"://llistat de tots els elements del directori (incloent els fitxers ocults)
                contentArray = dir.list();
                break;
            case "-la":
                String out1 = option_la(dir, content);
                contentArray = out1.split(",");
                break;
            default:
                String out2 = option_l(content, dir);
                contentArray = out2.split(",");
                break;
        }

        // return dir.list();
        return contentArray;
    }

    public String option_la(File dir, StringBuilder content) {
        //llistat de tots els elements i també les seves propietats (és a dir les dues opcions, -l i -a a la vegada)
        String out1 = "";
        File[] archivosYCarpetasInternos = dir.listFiles();
        for (File archivoOCarpeta : archivosYCarpetasInternos) {
            if (archivoOCarpeta.isHidden()) {
                content.append("\nName:");
                content.append(archivoOCarpeta.getName());
            } else {
                content.append("\nName:");
                content.append(archivoOCarpeta.getName());
            }
            content.append(" Permisos:");
            content.append(archivoOCarpeta.canRead() ? "+r" : "-r");
            content.append(archivoOCarpeta.canWrite() ? "+w" : "-w");
            content.append(archivoOCarpeta.canExecute() ? "+x" : "-x");
            content.append(" ");
            propietary(dir, content);
            java.util.Date data1 = new java.util.Date(dir.lastModified());
            content.append(" Data:");
            content.append(data1.toString());
            content.append(",");
            out1 = content.toString();
        }
        return out1;
    }

    public String option_l(StringBuilder content, File dir) {
        //ruta, permisos, propietari, data, nom.
        content.append("\nRuta: ");
        content.append(dir.getPath());
        content.append("\nPermisos: ");
        content.append(dir.canRead() ? "+r" : "-r");
        content.append(dir.canWrite() ? "+w" : "-w");
        content.append(dir.canExecute() ? "+x" : "-x");
        content.append("\n");
        propietary(dir, content);
        java.util.Date data = new java.util.Date(dir.lastModified());
        content.append("\nData: ");
        content.append(data.toString());
        content.append("\nName: ");
        content.append(dir.getName());
        content.append(",");
        String out = content.toString();
        return out;
    }

    public void propietary(File dir, StringBuilder content) {
        try {

            String fileName = dir.toString();
            Path path = Paths.get(fileName);
            UserPrincipal propietario = Files.getOwner(path);
            String username = propietario.getName();
            content.append("Propietari: ");
            content.append(propietario);
            // content.append(",");
        } catch (Exception e) {
        }
    }

}
