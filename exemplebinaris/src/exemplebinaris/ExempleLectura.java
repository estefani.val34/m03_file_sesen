/*
 cargo una imagen a otra pantalla 
 */
package exemplebinaris;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 *  Como hay dos mains en el programma tengo que elegir el main . Run> customize>ExempleLectura 
 * @author estefani
 */
public class ExempleLectura {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        CargaImagen imagen = new CargaImagen("tux.jpg");
        imagen.mostrar();
        
        
        File f = new File("exemple.bin");
        int x; // byte llegit 
        try {
            FileInputStream fis = new FileInputStream(f);
            //BufferedInputStream bis = new BufferedInputStream(fis);
            while ((x = fis.read()) != -1) { //while ((x = bis.read()) != -1) { // xa ir lleyendo mientras no ecnuetre un -1 y lo guardo en la x
                System.out.println("" + (byte) x); 
                
            }

            fis.close();//bis.close()
        } catch (IOException e) {
            e.printStackTrace();// imprima el error
        }

    }
}
