/*
 Escribe la list de bytes  a otro fichero 
 */
package exemplebinaris;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author estefani
 */
public class Exemplebinaris { //imagenes, videos con bytes 

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        File f = new File("exemple.bin"); //lo guarda en netbeansproject
        byte[] list = {10, 15, 15, 30, 45};

        try {// siempre hacer con un try-catch, 
            FileOutputStream fos = new FileOutputStream(f);
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            for (int i = 0; i < list.length; i++) {
                // fos.write(list[i]);
                bos.write(list[i]);
            }
            //fos.close();
            bos.close();
        } catch (IOException e) {

        }

    }


}
