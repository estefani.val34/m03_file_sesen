package exemplebinaris;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Copia un fichero a otro sitio y te abre la imagen. 
 * @author estefani
 */
public class practica5_1_3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Digues la ruta/nom de la imatge que volem copiar :");
        String pathDesde = sc.nextLine();

        System.out.println("Digues la ruta on volem copiar-la i també el nou nom de la imatge copiada:");
        String pathA = sc.nextLine();

        System.out.println("\nPath Origen:" +pathDesde+"\n" +"Path Destino: "+ pathA);

        try {
//            cp("/home/estefani/NetBeansProjects/m03_file_sesen/exemplebinaris/tux.jpg", "/home/estefani/tux.jpg");
//            CargaImagen imagen = new CargaImagen("tux.jpg");
//            imagen.mostrar();
            cp(pathDesde, pathA);
            CargaImagen imagen = new CargaImagen(pathA);
            imagen.mostrar();
        } catch (IOException ex) {
            Logger.getLogger(practica5_1_3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void cp(String archivoOrigen, String archivoDestino) throws IOException {
        FileInputStream copiarDesde = null;
        FileOutputStream copiarA = null;

        try {
            copiarDesde = new FileInputStream(archivoOrigen);
            copiarA = new FileOutputStream(archivoDestino);
            byte[] buffer = new byte[4096];
            int lecturaBytes;

            while ((lecturaBytes = copiarDesde.read(buffer)) != -1) {
                copiarA.write(buffer, 0, lecturaBytes);
            }
            System.out.println("El archivo se ha copiado correctamente.");
        } finally {
            if (copiarDesde != null)
            try {
                copiarDesde.close();
            } catch (IOException IOE) {

            }
            if (copiarA != null)
            try {
                copiarA.close();
            } catch (IOException IOE) {

            }
        }
    }
}
